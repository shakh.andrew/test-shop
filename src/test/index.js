const models = require("../app/models");
const Op = models.Sequelize.Op;
const wishlistModel = models.Wishlist;
const productModel = models.Product;
const customerModel = models.Customer;

module.exports = done => {
    wishlistModel.destroy({ where: { id: { [Op.gte]: 1 } } }).then(() => {
        const trancate = [
            //wishlistModel.destroy({ where: { id: { [Op.gte]: 1 } } }),
            customerModel.destroy({ where: { id: { [Op.gte]: 1 } } }),
            productModel.destroy({ where: { id: { [Op.gte]: 1 } } })
        ];
        Promise.all(trancate).then(() => {
            const c1 = customerModel.create({ first_name: "fname1", last_name: "lname1", email: "q1@site.com" });
            const c2 = customerModel.create({ first_name: "fname2", last_name: "lname2", email: "q2@site.com" });
            const c3 = customerModel.create({ first_name: "fname3", last_name: "lname3", email: "q3@site.com" });

            const p1 = productModel.create({ title: "product1", price: 99 });
            const p2 = productModel.create({ title: "product2", price: 55 });

            Promise.all([c1, c2, c3, p1, p2]).then(([c1, c2, c3, p1, p2]) => {
                const w = [];
                w.push(wishlistModel.create({ product_id: p1.id, customer_id: c1.id }));
                w.push(wishlistModel.create({ product_id: p1.id, customer_id: c1.id }));
                w.push(wishlistModel.create({ product_id: p1.id, customer_id: c2.id }));
                w.push(wishlistModel.create({ product_id: p1.id, customer_id: c2.id }));
                w.push(wishlistModel.create({ product_id: p1.id, customer_id: c3.id }));
                w.push(wishlistModel.create({ product_id: p1.id, customer_id: c3.id }));
                w.push(wishlistModel.create({ product_id: p1.id, customer_id: c3.id }));
                w.push(wishlistModel.create({ product_id: p1.id, customer_id: c3.id }));
                w.push(wishlistModel.create({ product_id: p2.id, customer_id: c1.id }));
                w.push(wishlistModel.create({ product_id: p2.id, customer_id: c1.id }));
                w.push(wishlistModel.create({ product_id: p2.id, customer_id: c1.id }));
                w.push(wishlistModel.create({ product_id: p2.id, customer_id: c1.id }));
                w.push(wishlistModel.create({ product_id: p2.id, customer_id: c2.id }));
                w.push(wishlistModel.create({ product_id: p2.id, customer_id: c2.id }));
                w.push(wishlistModel.create({ product_id: p2.id, customer_id: c3.id }));

                Promise.all(w).then(() => {
                    done();
                });
            });
        });
    });
};
