const assert = require("assert");
const wishlistDao = require("../app/api/v1/wishlist/wishlist.dao");

const chai = require("chai");
const chaiHttp = require("chai-http");
const cleanTables = require("./index");
const should = chai.should();

chai.use(chaiHttp);
describe("Test commad for cleaning up wishlist table", () => {
    before(done => {
        cleanTables(done);
    });

    describe("#countDuplicates()", () => {
        it("should return 9 dubbed data", async () => {
            const count = await wishlistDao.countDuplicates();
            assert.equal(count, 9);
        });
    });

    describe("#deleteDuplicates()", () => {
        it("should return 0 dubbed data", async () => {
            const count = await wishlistDao.countDuplicates();
            await wishlistDao.deleteDuplicates();
            assert.equal(count, 9);
        });
    });
});
