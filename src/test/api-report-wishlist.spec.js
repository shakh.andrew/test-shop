const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../app");
const cleanTables = require("./index");

chai.use(chaiHttp);
describe("API report wishlist", () => {
    before(done => {
        cleanTables(done);
    });

    describe("/GET report/wishlist", () => {
        it("it should GET all statistics data by products", done => {
            chai.request(server)
                .get("/report/wishlist")
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a("array");
                    res.body.length.should.be.eql(2);
                    done();
                });
        });
    });
});
