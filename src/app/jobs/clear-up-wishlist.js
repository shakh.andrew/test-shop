const wishListDao = require("../api/v1/wishlist/wishlist.dao");

wishListDao.deleteDuplicates().then(res => {
    console.log("The wishlist table was cleaned up:", res);
});

// wishListDao.countDuplicates().then(count => {
//     console.log("COUNT = ", count);
// });
