const dotenv = require("dotenv");
dotenv.config({ path: ".env" });

module.exports = {
    development: {
        username: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        dialect: "mariadb",
        define: {
            timestamps: false
        },
        dialectOptions: {
            useUTC: false,
            timezone: "Etc/GMT0"
        }
    },
    test: {
        username: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: `${process.env.DB_NAME}-test`,
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        dialect: "mariadb",
        define: {
            timestamps: false
        },
        dialectOptions: {
            useUTC: false,
            timezone: "Etc/GMT0"
        },
        logging: false
    },
    production: {
        username: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        dialect: "mariadb",
        define: {
            timestamps: false,
            underscored: true
        },
        dialectOptions: {
            useUTC: false,
            timezone: "Etc/GMT0"
        },
        logging: false
    }
};
