module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert(
            "products",
            [
                {
                    title: "Product title #1",
                    price: 32.12
                },
                {
                    title: "Product title #2",
                    price: 55
                },
                {
                    title: "Product title #3",
                    price: 100.67
                },
                {
                    title: "Product title #4",
                    price: 1.23
                },
                {
                    title: "Product title #5",
                    price: 9.99
                }
            ],
            {}
        );
    },

    down: (queryInterface, Sequelize) => {}
};
