module.exports = {
    up: async (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert(
            "customers",
            [
                {
                    first_name: "Super",
                    last_name: "Admin",
                    email: "root@site.com"
                },
                {
                    first_name: "Tony",
                    last_name: "Stark",
                    email: "tony65@averengers.org"
                },
                {
                    first_name: "Torvalds",
                    last_name: "Linus",
                    email: "torvalds69@kernel.org"
                },
                {
                    first_name: "Tim",
                    last_name: "Berners-Lee",
                    email: "tim55@sw3.org"
                },
                {
                    first_name: "Steve",
                    last_name: "Jobs",
                    email: "steve55@apple.com"
                },
                {
                    first_name: "Harry",
                    last_name: "Potter",
                    email: "harry89@pottermore.com"
                },
                {
                    first_name: "Andrew",
                    last_name: "Shakh",
                    email: "shakh.andrew@gmail.com"
                }
            ],
            {}
        );
    },

    down: (queryInterface, Sequelize) => {}
};
