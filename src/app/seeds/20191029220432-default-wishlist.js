"use strict";
const rand = require("../utils/random");

module.exports = {
    up: (queryInterface, Sequelize) => {
        const randomItems = [];

        for (let i = 0; i < 10000; i++) {
            randomItems.push({
                customer_id: rand.get(2, 6),
                product_id: rand.get(1, 5),
                created_at: rand.datetime("2019-01-01 00:00:00", "2019-10-29 23:59:59").format("YYYY-MM-DD HH:mm:ss")
            });
        }

        return queryInterface.bulkInsert("wishlist", randomItems, {});
    },

    down: (queryInterface, Sequelize) => {}
};
