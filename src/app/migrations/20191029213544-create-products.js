"use strict";
const DataTypes = require("sequelize/lib/data-types");
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable("products", {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true
            },
            title: {
                type: DataTypes.STRING,
                allowNull: false
            },
            price: {
                type: DataTypes.FLOAT,
                allowNull: false
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable("products");
    }
};
