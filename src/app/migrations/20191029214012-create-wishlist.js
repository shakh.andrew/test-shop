"use strict";
const DataTypes = require("sequelize/lib/data-types");
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface
            .createTable("wishlist", {
                id: {
                    type: DataTypes.INTEGER,
                    autoIncrement: true,
                    primaryKey: true
                },
                customer_id: {
                    type: DataTypes.INTEGER,
                    references: {
                        model: "customers",
                        key: "id"
                    },
                    allowNull: true
                },
                product_id: {
                    type: DataTypes.INTEGER,
                    references: {
                        model: "products",
                        key: "id"
                    },
                    allowNull: false
                },
                created_at: {
                    type: DataTypes.DATE,
                    allowNull: false,
                    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP")
                }
            })
            .then(() => {
                return queryInterface.addIndex("wishlist", ["created_at", "customer_id", "product_id"]);
            });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable("wishlist");
    }
};
