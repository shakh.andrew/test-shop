const sequelize = require("sequelize");
const models = require("../../../models");

/**
 * Gount of duplicates
 *
 * @returns {Promise<Model<any, any>[]>}
 */
module.exports.countDuplicates = () => {
    const sql = `
        SELECT (SUM(duplicates) - COUNT(duplicates)) AS duplicates 
        FROM
            (
                SELECT COUNT(*) AS duplicates 
                FROM wishlist
                GROUP BY customer_id, product_id
                HAVING COUNT(*) > 1
            ) as duplicates
    `;
    return models.sequelize.query(sql, { type: sequelize.QueryTypes.SELECT }).then(result => {
        return result[0] ? Number(result[0].duplicates) : 0;
    });
};

/**
 * Delete duplicates
 *
 * @returns {Promise<Model<any, any>[]>}
 */
module.exports.deleteDuplicates = () => {
    const sql = `
        DELETE FROM wishlist
        WHERE
            id NOT IN (
                SELECT MIN(id)
                FROM wishlist
                GROUP BY product_id, customer_id
        )
    `;
    return models.sequelize.query(sql);
};
