const express = require("express");
const router = express.Router();

router.use("/wishlist", require("./wishlist/wishlist.controller.js"));

module.exports = router;
