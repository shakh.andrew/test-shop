const models = require("../../../../models");
const wishlistModel = models.Wishlist;
const productModel = models.Product;
const customerModel = models.Customer;
const sequelize = require("sequelize");
const moment = require("moment");
const Op = sequelize.Op;

/**
 * Get by products
 *
 * @param start
 * @param end
 * @returns {Promise<Model<any, any>[]>}
 */
module.exports.getByProducts = (start = null, end = null) => {
    const config = {
        include: [
            {
                model: wishlistModel,
                as: "wishlist",
                required: true,
                attributes: []
            }
        ],
        attributes: ["id", "title", "price", [sequelize.fn("COUNT", sequelize.col("wishlist.id")), "count"]],
        group: [["id"]],
        order: [[sequelize.literal("`count`"), "DESC"]]
    };

    if (start && end) {
        config.include[0].where = {
            created_at: {
                [Op.between]: [`${start} 00:00:00`, `${end} 23:59:59`]
            }
        };
    }

    return productModel.findAll(config);
};
