const express = require("express");
const wishlistDao = require("./wishlist.dao");
const wishlistDto = require("./wishlist.dto");
const response = require("../../../../utils/response");
const router = express.Router();

/**
 * @swagger
 * /report/wishlist:
 *   get:
 *     description: Returns list of statistics data by products
 *     summary: list of statistics data by products
 *     tags:
 *       - report
 *     parameters:
 *       - name: start
 *         in: query
 *         description: Date start
 *         required: false
 *         type: string
 *         format: date
 *       - name: end
 *         in: query
 *         description: Date end
 *         required: false
 *         type: string
 *         format: date
 *     responses:
 *       200:
 *         description: statistic objects
 *         schema:
 *           type: array
 *           items:
 *             type: object
 *             properties:
 *               id:
 *                 type: integer
 *                 format: int64
 *               title:
 *                 type: string
 *               price:
 *                 type: number
 *               count:
 *                 type: integer
 */

router.get("/", (req, res) => {
    wishlistDao
        .getByProducts(req.query.start, req.query.end)
        .then(items => response.parametrizedSuccess(res, wishlistDto.byProducts(items)))
        .catch(err => response.failed(res, err));
});

module.exports = router;
