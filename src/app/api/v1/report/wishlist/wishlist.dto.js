/**
 * Return items for products report
 *
 * @param data
 * @returns {*}
 */
module.exports.byProducts = data => {
    let items = [];
    data.map(item => {
        items.push(itemByProduct(item));
    });
    return items;
};

function itemByProduct(model) {
    return {
        id: model.id ? model.id : null,
        title: model.title ? model.title : null,
        price: model.price ? model.price : null,
        count: model.dataValues.count ? model.dataValues.count : 0
    };
}
