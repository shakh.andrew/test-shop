module.exports = (sequelize, DataTypes) => {
    const Customer = sequelize.define(
        "Customer",
        {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true
            },
            first_name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            last_name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            email: {
                type: DataTypes.STRING,
                allowNull: false
            }
        },
        {
            classMethods: {},
            tableName: "customers",
            underscored: true
        }
    );

    Customer.associate = models => {
        // 1 - n
        Customer.hasMany(models.Wishlist, {
            as: "wishlist",
            foreignKey: "customer_id"
        });
    };

    return Customer;
};
