module.exports = (sequelize, DataTypes) => {
    const Product = sequelize.define(
        "Product",
        {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true
            },
            title: {
                type: DataTypes.STRING,
                allowNull: false
            },
            price: {
                type: DataTypes.FLOAT,
                allowNull: false
            }
        },
        {
            classMethods: {},
            tableName: "products",
            underscored: true
        }
    );

    Product.associate = models => {
        // 1 - n
        Product.hasMany(models.Wishlist, {
            as: "wishlist",
            foreignKey: "product_id"
        });
    };

    return Product;
};
