module.exports = (sequelize, DataTypes) => {
    const Wishlist = sequelize.define(
        "Wishlist",
        {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true
            },
            customer_id: {
                type: DataTypes.INTEGER,
                references: {
                    model: "customers",
                    key: "id"
                },
                allowNull: true
            },
            product_id: {
                type: DataTypes.INTEGER,
                references: {
                    model: "products",
                    key: "id"
                },
                allowNull: false
            },
            created_at: {
                type: DataTypes.DATE,
                allowNull: false,
                defaultValue: sequelize.literal("CURRENT_TIMESTAMP")
            }
        },
        {
            classMethods: {},
            tableName: "wishlist",
            underscored: true
        }
    );

    Wishlist.associate = models => {
        // 1 - 1
        Wishlist.belongsTo(models.Customer, {
            as: "customer",
            foreignKey: "customer_id"
        });
        Wishlist.belongsTo(models.Product, {
            as: "product",
            foreignKey: "product_id"
        });
    };

    return Wishlist;
};
