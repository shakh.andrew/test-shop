const momentRandom = require("moment-random");

/**
 * Generate random integer number from range
 *
 * @param min
 * @param max
 * @returns {number}
 */
module.exports.get = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
};

/**
 * Get random element of array
 *
 * @param arr
 * @returns {*}
 */
module.exports.any = arr => {
    return arr[this.get(0, arr.length - 1)];
};

/**
 * Get random date and time in range
 *
 * @param start
 * @param end
 */
module.exports.datetime = (start, end) => {
    return momentRandom(end, start);
};
