class ProjectError extends Error {
    constructor(message, name) {
        super(message);
        this.message = message;
        this.type = "projectError";
        this.name = name;
        this.error = message;
    }
}

module.exports.serverError = err => {
    const error = new ProjectError(err, "SERVER_ERROR");
    if (err) error.message = err;
    error.code = 500;
    return error;
};

module.exports.notFound = err => {
    const error = new ProjectError(err, "NOT_FOUND");
    if (err) error.message = err;
    error.code = 404;
    return error;
};

module.exports.badRequest = err => {
    const error = new ProjectError(err, "BAD_REQUEST");
    if (err) error.message = err;
    error.code = 400;
    return error;
};

module.exports.forbidden = err => {
    const error = new ProjectError(err, "FORBIDDEN");
    if (err) error.message = err;
    error.code = 403;
    return error;
};

module.exports.show = (err, res) => {
    let error = err;
    if (err.code == null) {
        error = this.serverError(err);
    }
    console.log("===== ERROR BEGIN =========================================================================");
    console.log(err);
    console.log("===== ERROR END =========================================================================");
    try {
        return res.status(error.code).json(error);
    } catch (e) {
        error.code = 500;
        return res.status(error.code).json(error);
    }
};

module.exports.invalidJoi = err => {
    let result = "";
    for (const error of err.details) {
        result += error.message + "; ";
    }
    return this.badRequest(result);
};
