#!/bin/bash
cp src/.env.example src/.env
npm --prefix ./src install
docker-compose up --build -d
sleep 60 | echo "sleeping 60"
docker ps -a
docker exec -i web ./node_modules/.bin/sequelize db:migrate
docker exec -i web ./node_modules/.bin/sequelize db:seed:all

docker exec -i db mysql -uroot -pqwerty < ./backup/create-test-db.sql
docker exec -i web npm run test-migrations
docker exec -i web npm test

echo "The end. Go to http://localhost:3000/";