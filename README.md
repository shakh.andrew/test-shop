### NODE MARIADB SKELETON

This is development environment (NOT FOR PRODUCTION). You should install [Docker](https://www.docker.com/) and docker compose

### Quick start:

-   run install script `$ bash ./install.sh`
-   the site will be accessible via the link [http://localhost:3000/](http://localhost:3000/)

### Extra commands (from your host machine):

-   for cleaning up duplicates from DB wishlist table run `$ docker exec -i web node app/jobs/clear-up-wishlist.js`
-   for run test `$ docker exec -i web npm test`

API documentation URL: [http://localhost:3000/api-docs](http://localhost:3000/api-docs)

---

A table contains information about products our customers have added to their Wishlist. Each record contains customer identifier and product information; there are 10+ million records. Each customer can have multiple products, but they all should be unique. We suspect some customers might have duplicate product records in the database. Find the best solution to remove the duplicates.

---

There are extra commands:

**Start all containers**

```sh
$ docker-compose up --build -d
```

**Stop all containers**

```sh
$ docker-compose down
```

**Login into container**

```sh
$ docker exec -it <name> bash
```

**List all containers**

```sh
$ docker ps -a
```

**Remove all images**

```sh
$ docker rmi $(docker images -q)
```

### Database

All db files are stored on host machine (safe mode). So you import a dump only one time and the data will be saved even if you stop or remove container.

**Import MySQL dump**

```sh
$ docker exec -i db-server mysql -uuser -puser databasename < ./backup/dump.sql
```

There is PhpMyAdmin container. URL for access to it: [http://localhost:8765](http://localhost:8765/)

**DB credentials:**

-   root / qwerty
-   user / user
